package LN;

import COMUN.itfProperty;

public class clsModelo implements itfProperty{
	
	private int id_modelo;
	private int a�o;
	private int potencia;
	private int num_puertas;
	private int cap_maletero;
	private String nombre;
	private String tipo;
	private int id_marca;
	private float consumo_urbano;
	private float consumo_extraurbano;
	
	public clsModelo() {
		this.id_modelo = id_modelo;
		this.a�o = a�o;
		this.nombre = nombre;
		this.potencia = potencia;
		this.num_puertas = num_puertas;
		this.cap_maletero = cap_maletero;
		this.tipo = tipo;
		this.id_marca = id_marca;
		this.consumo_urbano = consumo_urbano;
		this.consumo_extraurbano = consumo_extraurbano;
	}
	

	public int getId_modelo() {
		return id_modelo;
	}


	public void setId_modelo(int id_modelo) {
		this.id_modelo = id_modelo;
	}


	public int getA�o() {
		return a�o;
	}

	public void setA�o(int a�o) {
		this.a�o = a�o;
	}

	public int getPotencia() {
		return potencia;
	}

	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}

	public int getNum_puertas() {
		return num_puertas;
	}

	public void setNum_puertas(int num_puertas) {
		this.num_puertas = num_puertas;
	}

	public int getCap_maletero() {
		return cap_maletero;
	}

	public void setCap_maletero(int cap_maletero) {
		this.cap_maletero = cap_maletero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getId_marca() {
		return id_marca;
	}

	public void setId_marca(int id_marca) {
		this.id_marca = id_marca;
	}

	public float getConsumo_urbano() {
		return consumo_urbano;
	}

	public void setConsumo_urbano(float consumo_urbano) {
		this.consumo_urbano = consumo_urbano;
	}

	public float getConsumo_extraurbano() {
		return consumo_extraurbano;
	}

	public void setConsumo_extraurbano(float consumo_extraurbano) {
		this.consumo_extraurbano = consumo_extraurbano;
	}


	@Override
	public String toString() {
		return "clsModelo [id_modelo=" + id_modelo + ", a�o=" + a�o + ", potencia=" + potencia + ", num_puertas="
				+ num_puertas + ", cap_maletero=" + cap_maletero + ", nombre=" + nombre + ", tipo=" + tipo
				+ ", id_marca=" + id_marca + ", consumo_urbano=" + consumo_urbano + ", consumo_extraurbano="
				+ consumo_extraurbano + "]";
	}


	@Override
	public Object getProperty(String propiedad) {
		// TODO Auto-generated method stub
		
		switch(propiedad) {
		case "id_modelo": this.getId_modelo();
		case "a�o": this.getA�o();
		case "cap_maletero": this.getCap_maletero();
		case "con_extra": this.getConsumo_extraurbano();
		case "con_urbano": this.getConsumo_urbano();
		case "fk_id_marca": this.getId_marca();
		case "nombre": this.getNombre();
		case "num_puertas": this.getNum_puertas();
		case "potencia": this.getPotencia();
		case "tipo": this.getTipo();
		
		}
		return null;
	}
	

}
