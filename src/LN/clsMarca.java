package LN;

import COMUN.itfProperty;

public class clsMarca implements itfProperty{

	private int id_marca;
	private String nombre_marca;
	private String pais;
	private String tipo_gama;
	private String fundador;
	private int a�o_fundacion;
	
	public clsMarca() {
		this.id_marca = id_marca;
		this.nombre_marca = nombre_marca;
		this.pais = pais;
		this.tipo_gama = tipo_gama;
		this.fundador = fundador;
		this.a�o_fundacion = a�o_fundacion;
	}

	public int getId_marca() {
		return id_marca;
	}

	public void setId_marca(int id_marca) {
		this.id_marca = id_marca;
	}

	public String getNombre_marca() {
		return nombre_marca;
	}

	public void setNombre_marca(String nombre_marca) {
		this.nombre_marca = nombre_marca;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getTipo_gama() {
		return tipo_gama;
	}

	public void setTipo_gama(String tipo_gama) {
		this.tipo_gama = tipo_gama;
	}

	public String getFundador() {
		return fundador;
	}

	public void setFundador(String fundador) {
		this.fundador = fundador;
	}

	public int getA�o_fundacion() {
		return a�o_fundacion;
	}

	public void setA�o_fundacion(int a�o_fundacion) {
		this.a�o_fundacion = a�o_fundacion;
	}

	@Override
	public String toString() {
		return "clsMarca [id_marca=" + id_marca + ", nombre_marca=" + nombre_marca + ", pais=" + pais + ", tipo_gama="
				+ tipo_gama + ", fundador=" + fundador + ", a�o_fundacion=" + a�o_fundacion + "]";
	}

	@Override
	public Object getProperty(String propiedad) {
		// TODO Auto-generated method stub
		
		switch(propiedad) {
		case "id_marca": return this.getId_marca();
		case "fundador": return this.getFundador();
		case "nombre": return this.getNombre_marca();
		case "a�o_fundacion": return this.getA�o_fundacion();
		case "pais": return this.getPais();
		case "tipo_gama": return this.getTipo_gama();
		}
		return null;
	}
	
	
}
