package LN;

import COMUN.itfProperty;

public class clsTrabajador extends clsPersona implements itfProperty{
	
	private float salario;
	private String jornada;
	private int a�os_antiguedad;
	
	public clsTrabajador() {
		super();
		this.salario = salario;
		this.jornada = jornada;
		this.a�os_antiguedad = a�os_antiguedad;
	}

	public float getSalario() {
		return salario;
	}

	public void setSalario(float salario) {
		this.salario = salario;
	}

	public String getJornada() {
		return jornada;
	}

	public void setJornada(String jornada) {
		this.jornada = jornada;
	}

	public int getA�os_antiguedad() {
		return a�os_antiguedad;
	}

	public void setA�os_antiguedad(int a�os_antiguedad) {
		this.a�os_antiguedad = a�os_antiguedad;
	}

	@Override
	public String toString() {
		return "clsTrabajador [salario=" + salario + ", jornada=" + jornada + ", a�os_antiguedad=" + a�os_antiguedad
				+ ", getSalario()=" + getSalario() + ", getJornada()=" + getJornada() + ", getA�os_antiguedad()="
				+ getA�os_antiguedad() + ", getNombre()=" + getNombre() + ", getApellido()=" + getApellido()
				+ ", getPais_nacimiento()=" + getPais_nacimiento() + ", getTelefono()=" + getTelefono()
				+ ", getFecha_nacimiento()=" + getFecha_nacimiento() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

	@Override
	public Object getProperty(String propiedad) {
		// TODO Auto-generated method stub
		switch(propiedad) {
		case "dni": return this.getDNI();
		case "nombre": return this.getNombre();
		}
		return null;
	}
	
	

}
