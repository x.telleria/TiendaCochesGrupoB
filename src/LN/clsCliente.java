package LN;

import COMUN.itfProperty;

public class clsCliente extends clsPersona implements itfProperty{
	
	private int num_compras;
	private String num_bancario;
	
	public clsCliente() {
		super();
		this.num_bancario = num_bancario;
		this.num_compras = num_compras;
	}

	public int getNum_compras() {
		return num_compras;
	}

	public void setNum_compras(int num_compras) {
		this.num_compras = num_compras;
	}

	public String getNum_bancario() {
		return num_bancario;
	}

	public void setNum_bancario(String num_bancario) {
		this.num_bancario = num_bancario;
	}

	@Override
	public String toString() {
		return "clsCliente [num_compras=" + num_compras + ", num_bancario=" + num_bancario + ", getNum_compras()="
				+ getNum_compras() + ", getNum_bancario()=" + getNum_bancario() + ", getNombre()=" + getNombre()
				+ ", getApellido()=" + getApellido() + ", getPais_nacimiento()=" + getPais_nacimiento()
				+ ", getTelefono()=" + getTelefono() + ", getFecha_nacimiento()=" + getFecha_nacimiento()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}

	@Override
	public Object getProperty(String propiedad) {
		// TODO Auto-generated method stub
		
		switch(propiedad) {
		
		case "dni": return this.getDNI();
		case "nombre": return this.getNombre();
		case "apellido": return this.getApellido();
		case "fecha_nacimiento": return this.getFecha_nacimiento();
		case "pais_nacimiento": return this.getPais_nacimiento();
		case "telefono": return this.getTelefono();
		case "num_bancario": return this.getNum_bancario();
		case "num_compras": return this.getNum_compras();
		
		}
		return null;
		
	}

	

}
