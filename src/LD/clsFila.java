package LD;

import java.util.HashMap;

import COMUN.itfData;

public class clsFila implements itfData{
	
	private HashMap<Integer, Object> fila;
	
	public clsFila() 
	{
		fila = new HashMap<Integer, Object>();
	}
	
	public void ponerColumna (Integer columna, Object valor) {
		
		fila.put(columna, valor);
	}
	

	@Override
	public Object getData (Integer columna) {
		
		return this.fila.get(columna);
	}

	
}
