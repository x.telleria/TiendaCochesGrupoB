package LP;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;

import COMUN.itfProperty;
import LN.clsGestorLN;
import LN.clsMarca;
import LN.clsModelo;
import LN.clsSucursal;

public class clsMenuPrivado {
	
	int eleccionMenuPrivado;
	clsGestorLN gestorLN;
	
	public clsMenuPrivado() {
		
		gestorLN = new clsGestorLN();
		
	}
	
	public void menuPrivadoAcceso(String usuario, String contrase�a) {
		
		if (usuario == "user1") {
			if(contrase�a == "contra1") {
				menuPrivado();
			}else System.out.println("Contrase�a incorrecta");
			
		}else System.out.println("User incorrecto");
	}
	
	public void menuPrivado() {
		
		System.out.println("1. Alta de marca");
		System.out.println("2. Alta de modelo");
		System.out.println("3. Alta de sucursal");
		System.out.println("4. Alta de cliente");
		System.out.println("5. Alta de coche");
		System.out.println("6. Alta de trabajador");
		System.out.println("7. Alta de proveedor");
		System.out.println("7. Borrado de modelo");
		System.out.println("7. Borrado de proveedor");
		System.out.println("7. Borrado de cliente");
		
		eleccionMenuPrivado = clsUtilidadesLP.leerEntero();
		
		switch(eleccionMenuPrivado) {
		case 3: 
			altaSucursal();
			break;
		case 2:
			AltaModelo();
			break;
		case 1: 
			AltaMarca();
			break;
		case 4:
			altaCliente();
			break;
		case 5:
			altaCoche();
			break;
		case 6:
			
			break;
		case 7:
			altaProveedor();
			break;
		}
		
	}
	
	public void ElimiTrabajador() {
		
		System.out.println("Introduce el dni");
		String dni = clsUtilidadesLP.leerCadena();
		
		gestorLN.eliminarTrabajadorLN(dni);
	}
	
	public void ElimiCoche() {
		
		System.out.println("Introduce la matricula");
		String matricula = clsUtilidadesLP.leerCadena();
		
		gestorLN.eliminarCocheLN(matricula);
	}
	
	public void ElimiProveedor() {
		
		System.out.println("Introduce el nombre del proveedor");
		String proveedor = clsUtilidadesLP.leerCadena();
	
	}
	
	public void AltaMarca() {
		
		System.out.println("Introduce el nombre");
		String nombre = clsUtilidadesLP.leerCadena();
		System.out.println("Introduce el a�o de fundaci�n");
		int a�o_fundacion = clsUtilidadesLP.leerEntero();
		System.out.println("Introduce el pais");
		String pais = clsUtilidadesLP.leerCadena();
		System.out.println("Introduce el fundador");
		String fundador = clsUtilidadesLP.leerCadena();
		System.out.println("Introduce el tipo de gama");
		String tipo_gama = clsUtilidadesLP.leerCadena();
		
		gestorLN.altaMarcaLN(a�o_fundacion, fundador, nombre, pais, tipo_gama);
		
	}
	
	public void AltaModelo() {
		
		clsMarca marca = new clsMarca();
		
		System.out.println("Introduce el nombre");
		String nombre = clsUtilidadesLP.leerCadena();
		System.out.println("Introduce el a�o de lanzamiento");
		int a�o_lanz = clsUtilidadesLP.leerEntero();
		System.out.println("Introduce el tipo de vehiculo");
		String tipo = clsUtilidadesLP.leerCadena();
		System.out.println("Introduce el potencia");
		int potencia = clsUtilidadesLP.leerEntero();
		System.out.println("Introduce el numero de puertas");
		int num_puertas = clsUtilidadesLP.leerEntero();
		System.out.println("Introduce el nombre");
		int cap_mal = clsUtilidadesLP.leerEntero();
		System.out.println("Introduce el nombre");
		Float con_urbano = clsUtilidadesLP.leerFloat();
		System.out.println("Introduce el nombre");
		Float con_extra = clsUtilidadesLP.leerFloat();
		System.out.println("Introduce el nombre");
		String nombre_marca = clsUtilidadesLP.leerCadena();
		
		marca = gestorLN.verMarcaByNombre(nombre_marca);
		
		int id_marca = (int)marca.getProperty("id_marca");
		
		gestorLN.altaModelo(nombre, a�o_lanz, tipo, potencia, num_puertas, cap_mal, con_urbano, con_extra, id_marca);
		
		
	}
	
	public void altaCliente() {
		
		System.out.println("ALTA DE NUEVO CLIENTE");
		System.out.println("Introducir el DNI->");
		String dni = clsUtilidadesLP.leerCadena();
		System.out.println("Introducur el nombre");
		String nombre = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir el apellido");
		String apellido = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir el telefono ->");
		int telefono = clsUtilidadesLP.leerEntero();
		System.out.println("Introducir el pais de nacimiento");
		String pais_nacimiento = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir la fecha de nacimiento");
		String fecha = clsUtilidadesLP.leerCadena();
		LocalDate fecha_nacimiento= java.time.LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd-MM-yyyy") );
		System.out.println("Introducir el numero de compras");
		int num_compras = clsUtilidadesLP.leerEntero();
		System.out.println("Introducir el numero bancario");
		String num_bancario = clsUtilidadesLP.leerCadena();
		
		gestorLN.altaClienteLN(dni, nombre, apellido, telefono, pais_nacimiento, fecha_nacimiento, num_bancario, num_compras);
		
	}
	
	public void altaProveedor() {
		
		System.out.println("ALTA DE NUEVOS PROVEEDORES");
		System.out.println("Introduce el id ->");
		int id = clsUtilidadesLP.leerEntero();
		System.out.println("Introduce el nombre ->");
		String nombre = clsUtilidadesLP.leerCadena();
		System.out.println("Introduce el telefono ->");
		int telefono = clsUtilidadesLP.leerEntero();
		System.out.println("Introduce el mail ->");
		String mail = clsUtilidadesLP.leerCadena();
		
		
		gestorLN.AltaProveedorLN(id, nombre, telefono, mail);
		
	}
	
	public void altaCoche() {
		
		//clsModelo modelo = new clsModelo();
		HashSet <itfProperty> sucursal = new HashSet <itfProperty>();
		
		
		
		System.out.println("Introducir la matricula");
		String matricula = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir la fecha de creaci�n:");
		String fecha = clsUtilidadesLP.leerCadena();
		LocalDate a�o= java.time.LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd-MM-yyyy") );
		System.out.println("Introducir los kilometros");
		int kilometros = clsUtilidadesLP.leerEntero();
		System.out.println("Introducir el color");
		String color = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir el tipo de cambio");
		String tipo_cambio = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir la ciudad de la sucursal");
		String ciudad_suc = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir el nombre del modelo");
		int id_modelo = clsUtilidadesLP.leerEntero();
		System.out.println("Introducir el id proveedor");
		int id_prov = clsUtilidadesLP.leerEntero();
		System.out.println("Introducir el dni del cliente");
		String fk_dni_cli = clsUtilidadesLP.leerCadena();
		
		
		sucursal = gestorLN.verSucursalByCiudad(ciudad_suc);
		for (itfProperty prop : sucursal) {
			int id_sucursal = (int)prop.getProperty("id_sucursal");
			
			gestorLN.altaCocheLN(matricula, a�o, kilometros, color, tipo_cambio, id_sucursal, id_modelo, id_prov, fk_dni_cli);
		}
		

		
	}
	
	
	public void altaSucursal() {
		
		System.out.println("ALTA DE NUEVA SUCURSAL");
		
		System.out.println("Introducur la ciudad");
		String ciudad = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir el telefono ->");
		int telefono = clsUtilidadesLP.leerEntero();
		System.out.println("Introducir el mail");
		String mail = clsUtilidadesLP.leerCadena();
		
		
		gestorLN.altaSucursalLN(ciudad, telefono, mail);
					
	  
	}
	

}
