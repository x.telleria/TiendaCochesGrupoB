package LP;



import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;

import COMUN.itfProperty;
import LN.clsCoche;
import LN.clsGestorLN;
import LN.clsMarca;
import LN.clsModelo;

public class clsMenuPrincipal {
	
	int eleccionMenu;
	private clsGestorLN gestorLN;
	private clsMenuPrivado menupriv;
	
	public clsMenuPrincipal() {
		
		gestorLN = new clsGestorLN();
		menupriv = new clsMenuPrivado();
	}
	
	
	public void menuPrincipal() {
		
		
		while (eleccionMenu != 99) {
		System.out.println("Bienvenido al menu principal.");
		System.out.println("Escoge la opci�n que desees.");
		System.out.println("1.Ver modelos de coche");
		System.out.println("2.Ver marcas por nombre");
		System.out.println("4.Ver sucursal de una ciudad");
		System.out.println("5.Alta proveedor");
		System.out.println("6.Ver proveedores");
		System.out.println("7.Alta de sucursal");
		System.out.println("8.Ver trabajador by sucursal");
		System.out.println("9.Ver trabajadores con DNI");
		System.out.println("10. Ver todos los coches");
		System.out.println("11.Ver coche con matr�cula");
		System.out.println("12.Ver modelo con nombre");
		System.out.println("13.A�adir modelo");
		System.out.println("14.Acceder menu privado");
		System.out.println("15.Alta de cliente");
		System.out.println("16.Ver cliente");
		System.out.println("99.Salir");
		
		
		eleccionMenu = clsUtilidadesLP.leerEntero();
		
		
		switch(eleccionMenu) {
		
		case 1: 
			
			break;
		case 2:
			verMarcaByNombre();
			break;
		case 4:
			verSucursal();
			break;
		case 5: 
			altaProveedor();
			break;
		case 6: 
			verProveedores();
			break;
		case 7: 
			altaSucursal();
			break;
		case 8: 
			verTrabajadorBySucursal();
			break;
		case 9:
			verTrabajadorByDNI();
			break;
		case 10:
			verCoche();
			break;
		case 11:
			verCocheByMatricula();
			break;
		case 12:
			verModelo();
			break;
		case 14:
			accedermenup();
			break;
		case 15: 
			altaCliente();
			break;
		case 16: 
			verCliente();
		case 99:
			System.out.println("Has salido de la APP.");
			
		}
		}
	}
	
	public void accedermenup() {
		
		ArrayList<itfProperty> prop = new ArrayList<itfProperty>();
		String password = "";
		String username = "";
		
		System.out.println("Usuario:");
		String usuario = clsUtilidadesLP.leerCadena();
		System.out.println("Contrase�a:");
		String contrase�a = clsUtilidadesLP.leerCadena();
		
		prop = gestorLN.AccesoPrivado(usuario);
		
		for(itfProperty fila : prop) {
			
			username = (String)fila.getProperty("user");
			password = (String)fila.getProperty("pass");
			
			System.out.println(username + password+ "//"+ usuario + contrase�a );
			
			if(usuario.equals(username)) {
				if(password.equals(contrase�a)) {
					System.out.println("Usuario validado!");
					menupriv.menuPrivado();
				}
			}System.out.println("Usuario incorrecto");
		}	
	}
	
	public void altaProveedor() {
		
		System.out.println("ALTA DE NUEVOS PROVEEDORES");
		System.out.println("Introduce el id ->");
		int id = clsUtilidadesLP.leerEntero();
		System.out.println("Introduce el nombre ->");
		String nombre = clsUtilidadesLP.leerCadena();
		System.out.println("Introduce el telefono ->");
		int telefono = clsUtilidadesLP.leerEntero();
		System.out.println("Introduce el mail ->");
		String mail = clsUtilidadesLP.leerCadena();
		
		
		gestorLN.AltaProveedorLN(id, nombre, telefono, mail);
		
	}
	
	public void verProveedores() {
		
		gestorLN.VerProveedoresLN();
		
	}
	
	public void altaSucursal() {
		
		System.out.println("ALTA DE NUEVA SUCURSAL");
		
		System.out.println("Introducur la ciudad");
		String ciudad = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir el telefono ->");
		int telefono = clsUtilidadesLP.leerEntero();
		System.out.println("Introducir el mail");
		String mail = clsUtilidadesLP.leerCadena();
		
		
		gestorLN.altaSucursalLN(ciudad, telefono, mail);
					
	  
	}
	
	public void verSucursal() {
		
		HashSet<itfProperty> Hash = new HashSet<itfProperty>();
		
		System.out.println("Introduce la ciudad:");
		String ciudad = clsUtilidadesLP.leerCadena();
		
		Hash = gestorLN.verSucursalByCiudad(ciudad);
		
		for(itfProperty prop : Hash) {
			System.out.println("id_sucursal: "+ prop.getProperty("id_sucursal"));
			System.out.println("ciudad: "+ prop.getProperty("ciudad"));
			System.out.println("telefono: "+ prop.getProperty("telefono"));
			System.out.println("mail: "+ prop.getProperty("mail"));
		}
		
		
	}
	
	public void verTrabajadorBySucursal() {
		
		HashSet<itfProperty> Hash = new HashSet<itfProperty>();
		
		System.out.println("Introduce la ciudad:");
		String ciudad = clsUtilidadesLP.leerCadena();
		
		Hash = gestorLN.verTrabajadorBySucursal(ciudad);
		
		for(itfProperty prop : Hash) {
			System.out.println("dni: "+ prop.getProperty("dni"));
			System.out.println("nombre: "+ prop.getProperty("nombre"));

		}
		
		
	}
	public void altaCliente() {
		
		System.out.println("ALTA DE NUEVO CLIENTE");
		System.out.println("Introducir el DNI->");
		String dni = clsUtilidadesLP.leerCadena();
		System.out.println("Introducur el nombre");
		String nombre = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir el apellido");
		String apellido = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir el telefono ->");
		int telefono = clsUtilidadesLP.leerEntero();
		System.out.println("Introducir el pais de nacimiento");
		String pais_nacimiento = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir la fecha de nacimiento");
		String fecha = clsUtilidadesLP.leerCadena();
		LocalDate fecha_nacimiento= java.time.LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd-MM-yyyy") );
		System.out.println("Introducir el numero de compras");
		int num_compras = clsUtilidadesLP.leerEntero();
		System.out.println("Introducir el numero bancario");
		String num_bancario = clsUtilidadesLP.leerCadena();
		
		gestorLN.altaClienteLN(dni, nombre, apellido, telefono, pais_nacimiento, fecha_nacimiento, num_bancario, num_compras);
		
	}
	
	public void altaTrabajador() {
		
		System.out.println("ALTA DE NUEVO TRABAJADOR");
		System.out.println("Introducir el DNI->");
		String dni = clsUtilidadesLP.leerCadena();
		System.out.println("Introducur el nombre");
		String nombre = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir el apellido");
		String apellido = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir el telefono ->");
		int telefono = clsUtilidadesLP.leerEntero();
		System.out.println("Introducir el pais de nacimiento");
		String pais_nacimiento = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir la fecha de nacimiento");
		String fecha = clsUtilidadesLP.leerCadena();
		LocalDate fecha_nacimiento= java.time.LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd-MM-yyyy") );
		System.out.println("Introducir el salario");
		float salario = clsUtilidadesLP.leerFloat();
		System.out.println("Introducir la jornada");
		String jornada = clsUtilidadesLP.leerCadena();
		System.out.println("Introducir a�os de antiguedad");
		int a�os_antiguedad = clsUtilidadesLP.leerEntero();
		
		gestorLN.altaTrabajadorLN(dni, nombre, apellido, telefono, pais_nacimiento, fecha_nacimiento, salario, jornada, a�os_antiguedad);
	}
	
	public void verCoche() {
		
		ArrayList<itfProperty> prop = new ArrayList<itfProperty>();
		
		prop = gestorLN.VerCoche();
		
		for(itfProperty fila : prop) {
			
			System.out.println("Matricula "+ fila.getProperty("matricula"));
			System.out.println("Kilometros "+ fila.getProperty("kilometros"));
			System.out.println("color "+ fila.getProperty("color"));
			System.out.println("tipo cambio "+ fila.getProperty("tipo_cambio"));
			System.out.println("id_modelo "+ fila.getProperty("id_modelo"));
			System.out.println("id_proveedor "+ fila.getProperty("id_proveedor"));
			System.out.println("id_sucursal "+ fila.getProperty("id_sucursal"));
			System.out.println("dni "+ fila.getProperty("dni"));
		}
		
		
	}
	
	public void verTrabajadorByDNI() {
		
		System.out.println("Introducir el DNI ->");
		String dni = clsUtilidadesLP.leerCadena();
		
		gestorLN.verTrabajador(dni);
	}
	
	
	public void verCocheByMatricula() {
		
		clsCoche coche = new clsCoche();
		System.out.println("Introduce la matr�cula ->");
		String matricula = clsUtilidadesLP.leerCadena();
		
		coche = gestorLN.verCocheByMatricula(matricula);
		
		System.out.println(coche.getProperty("matricula"));
		System.out.println(coche.getProperty("kilometros"));
		System.out.println(coche.getProperty("color"));
		System.out.println(coche.getProperty("tipo_cambio"));
		System.out.println(coche.getProperty("id_sucursal"));
		System.out.println(coche.getProperty("id_modelo"));
		
	}
	

	
	public void verMarcaByNombre() {
		
		ArrayList<itfProperty> fila = new ArrayList<itfProperty>();
		clsMarca marca = new clsMarca();
		
		System.out.println("Introduce el nombre de la marca.");
		String nombre_marca = clsUtilidadesLP.leerCadena();
		
		marca = gestorLN.verMarcaByNombre(nombre_marca);
		
		System.out.println(marca.getProperty("id_marca"));
		System.out.println(marca.getProperty("nombre"));
		System.out.println(marca.getProperty("tipo_gama"));
		System.out.println(marca.getProperty("pais"));
		System.out.println(marca.getProperty("fundador"));
		System.out.println(marca.getProperty("a�o_fundacion"));
		
	
	}
	
	/**
	 * CLIENTE ITFPROP
	 */
	
	public void verCliente() {
		
		ArrayList<itfProperty> cli = new ArrayList<itfProperty>();
		
		System.out.println("Introduce el DNI -->");
		String dni = clsUtilidadesLP.leerCadena();
		
		cli = gestorLN.BuscarCliente(dni);
		
		for(itfProperty filaProp : cli) {
			
			System.out.println(filaProp.getProperty("dni"));
			System.out.println(filaProp.getProperty("nombre"));
			System.out.println(filaProp.getProperty("apellido"));
			System.out.println(filaProp.getProperty("telefono"));
			System.out.println(filaProp.getProperty("pais_nacimiento"));
			System.out.println(filaProp.getProperty("fecha_nacimiento"));
			System.out.println(filaProp.getProperty("num_compras"));
			System.out.println(filaProp.getProperty("num_bancario"));
		}
		
	}
	
	public void verModelo() {
		
		ArrayList<itfProperty> prop = new ArrayList<itfProperty>();
		
		System.out.println("Introduce el nombre del modelo");
		String nombre = clsUtilidadesLP.leerCadena();
		
		prop = gestorLN.BuscarModeloPorNombre(nombre);
		
		for(itfProperty modelo : prop) {
		System.out.println(modelo.getProperty("id_modelo"));
		System.out.println(modelo.getProperty("a�o"));
		System.out.println(modelo.getProperty("cap_maletero"));
		System.out.println(modelo.getProperty("con_urbano"));
		System.out.println(modelo.getProperty("con_extra"));
		System.out.println(modelo.getProperty("nombre"));
		System.out.println(modelo.getProperty("num_puertas"));
		System.out.println(modelo.getProperty("potencia"));
		System.out.println(modelo.getProperty("tipo"));
		System.out.println(modelo.getProperty("fk_id_marca"));
		}
	}
	
	

	

}
