-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema tiendaelectro
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `tiendaelectro` ;

-- -----------------------------------------------------
-- Schema tiendaelectro
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tiendaelectro` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema tiendacoches
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `tiendacoches` ;

-- -----------------------------------------------------
-- Schema tiendacoches
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tiendacoches` DEFAULT CHARACTER SET utf8 ;
USE `tiendaelectro` ;

-- -----------------------------------------------------
-- Table `tiendaelectro`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendaelectro`.`cliente` (
  `id_cliente` INT(11) NOT NULL AUTO_INCREMENT,
  `dni` VARCHAR(9) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `fecha_nacimiento` DATE NOT NULL,
  `ciudad` VARCHAR(45) NOT NULL,
  `telefono` INT(11) NOT NULL,
  `calle` VARCHAR(60) NOT NULL,
  `num_portal` INT(11) NOT NULL,
  `num_piso` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id_cliente`))
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendaelectro`.`electrodomesticos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendaelectro`.`electrodomesticos` (
  `id_electrodomestico` INT(11) NOT NULL AUTO_INCREMENT,
  `tipo_producto` VARCHAR(45) NOT NULL,
  `marca` VARCHAR(45) NOT NULL,
  `modelo` VARCHAR(45) NOT NULL,
  `precio` INT(11) NULL DEFAULT NULL,
  `color` VARCHAR(45) NOT NULL,
  `unidades` INT(11) NOT NULL,
  `descripcion` VARCHAR(90) NOT NULL,
  PRIMARY KEY (`id_electrodomestico`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendaelectro`.`sede`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendaelectro`.`sede` (
  `id_sede` INT(11) NOT NULL AUTO_INCREMENT,
  `provincia` VARCHAR(60) NOT NULL,
  `mcuadrados` VARCHAR(45) NOT NULL,
  `ciudad` VARCHAR(45) NOT NULL,
  `calle` VARCHAR(45) NOT NULL,
  `num_portal` INT(11) NOT NULL,
  `trabajadores` INT(11) NOT NULL,
  PRIMARY KEY (`id_sede`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendaelectro`.`repartidor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendaelectro`.`repartidor` (
  `id_repartidor` INT(11) NOT NULL AUTO_INCREMENT,
  `dni` VARCHAR(9) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `fecha_nacimiento` DATE NOT NULL,
  `calle` VARCHAR(45) NOT NULL,
  `ciudad` VARCHAR(45) NOT NULL,
  `num_portal` INT(11) NOT NULL,
  `num_piso` VARCHAR(10) NOT NULL,
  `fk_id_sede` INT(11) NOT NULL,
  PRIMARY KEY (`id_repartidor`),
  INDEX `fk_Repartidor_Sede1_idx` (`fk_id_sede` ASC) VISIBLE,
  CONSTRAINT `fk_Repartidor_Sede1`
    FOREIGN KEY (`fk_id_sede`)
    REFERENCES `tiendaelectro`.`sede` (`id_sede`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendaelectro`.`pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendaelectro`.`pedido` (
  `id_pedido` INT(11) NOT NULL AUTO_INCREMENT,
  `precio` INT(11) NULL DEFAULT NULL,
  `cantidad_productos` INT(11) NOT NULL,
  `fk_id_repartidor` INT(11) NOT NULL,
  `fk_id_cliente` INT(11) NOT NULL,
  PRIMARY KEY (`id_pedido`),
  INDEX `fk_Pedido_Repartidor1_idx` (`fk_id_repartidor` ASC) VISIBLE,
  INDEX `fk_Pedido_Cliente1_idx` (`fk_id_cliente` ASC) VISIBLE,
  CONSTRAINT `fk_Pedido_Cliente1`
    FOREIGN KEY (`fk_id_cliente`)
    REFERENCES `tiendaelectro`.`cliente` (`id_cliente`),
  CONSTRAINT `fk_Pedido_Repartidor1`
    FOREIGN KEY (`fk_id_repartidor`)
    REFERENCES `tiendaelectro`.`repartidor` (`id_repartidor`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendaelectro`.`proveedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendaelectro`.`proveedor` (
  `id_proveedor` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre_empresa` VARCHAR(45) NOT NULL,
  `telefono` INT(11) NOT NULL,
  `ciudad` VARCHAR(45) NOT NULL,
  `pais` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_proveedor`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendaelectro`.`venta_electrodomesticos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendaelectro`.`venta_electrodomesticos` (
  `id_venta_electrodomestico` INT(11) NOT NULL AUTO_INCREMENT,
  `fk_id_proveedor` INT(11) NOT NULL,
  `fk_id_pedido` INT(11) NOT NULL,
  `fk_id_electrodomestico` INT(11) NOT NULL,
  PRIMARY KEY (`id_venta_electrodomestico`),
  INDEX `fk_Electrodomesticos_Proveedores1_idx` (`fk_id_proveedor` ASC) VISIBLE,
  INDEX `fk_Electrodomesticos_Pedido1_idx` (`fk_id_pedido` ASC) VISIBLE,
  INDEX `fk_venta_electrodomesticos_electrodomesticos1_idx` (`fk_id_electrodomestico` ASC) VISIBLE,
  CONSTRAINT `fk_Electrodomesticos_Pedido1`
    FOREIGN KEY (`fk_id_pedido`)
    REFERENCES `tiendaelectro`.`pedido` (`id_pedido`),
  CONSTRAINT `fk_Electrodomesticos_Proveedores1`
    FOREIGN KEY (`fk_id_proveedor`)
    REFERENCES `tiendaelectro`.`proveedor` (`id_proveedor`),
  CONSTRAINT `fk_venta_electrodomesticos_electrodomesticos1`
    FOREIGN KEY (`fk_id_electrodomestico`)
    REFERENCES `tiendaelectro`.`electrodomesticos` (`id_electrodomestico`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;

USE `tiendacoches` ;

-- -----------------------------------------------------
-- Table `tiendacoches`.`acceso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendacoches`.`acceso` (
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendacoches`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendacoches`.`cliente` (
  `DNI` VARCHAR(9) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `telefono` INT(11) NOT NULL,
  `pais_residencia` VARCHAR(45) NOT NULL,
  `fecha_nacimiento` DATE NOT NULL,
  `num_bancario` VARCHAR(24) NOT NULL,
  `num_compras` INT NOT NULL,
  PRIMARY KEY (`DNI`),
  UNIQUE INDEX `DNI_UNIQUE` (`DNI` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendacoches`.`marca`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendacoches`.`marca` (
  `id_marca` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre_marca` VARCHAR(45) NOT NULL,
  `pais` VARCHAR(45) NOT NULL,
  `tipo_gama` VARCHAR(45) NOT NULL,
  `fundador` VARCHAR(45) NOT NULL,
  `año_fundacion` INT(11) NOT NULL,
  PRIMARY KEY (`id_marca`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendacoches`.`modelo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendacoches`.`modelo` (
  `id_modelo` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre_modelo` VARCHAR(55) NOT NULL,
  `año_lanzamiento` VARCHAR(45) NOT NULL,
  `tipo_vehiculo` VARCHAR(45) NOT NULL,
  `potencia` INT(11) NOT NULL,
  `num_puertas` INT(11) NOT NULL,
  `cap_maletero` INT(11) NOT NULL,
  `consumo_urbano` FLOAT NOT NULL,
  `consumo_extraurbano` FLOAT NOT NULL,
  `FK_id_marca` INT(11) NOT NULL,
  PRIMARY KEY (`id_modelo`),
  INDEX `fk_MODELO_MARCA_idx` (`FK_id_marca` ASC) VISIBLE,
  CONSTRAINT `fk_MODELO_MARCA`
    FOREIGN KEY (`FK_id_marca`)
    REFERENCES `tiendacoches`.`marca` (`id_marca`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendacoches`.`proveedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendacoches`.`proveedor` (
  `id_proveedor` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `telefono` INT(11) NOT NULL,
  `mail` VARCHAR(65) NOT NULL,
  PRIMARY KEY (`id_proveedor`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendacoches`.`sucursal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendacoches`.`sucursal` (
  `id_sucursal` INT(11) NOT NULL AUTO_INCREMENT,
  `ciudad` VARCHAR(45) NOT NULL,
  `telefono` INT(11) NOT NULL,
  `mail` VARCHAR(55) NOT NULL,
  PRIMARY KEY (`id_sucursal`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendacoches`.`coche`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendacoches`.`coche` (
  `matricula` VARCHAR(7) NOT NULL,
  `año` DATE NOT NULL,
  `kilometros` INT(11) NOT NULL,
  `color` VARCHAR(45) NOT NULL,
  `tipo_cambio` VARCHAR(45) NOT NULL,
  `FK_id_modelo` INT(11) NOT NULL,
  `FK_id_proveedor` INT(11) NOT NULL,
  `FK_id_sucursal` INT(11) NOT NULL,
  `FK_CLIENTE_DNI` VARCHAR(9) NULL,
  PRIMARY KEY (`matricula`),
  UNIQUE INDEX `matricula_UNIQUE` (`matricula` ASC) VISIBLE,
  INDEX `fk_COCHE_MODELO1_idx` (`FK_id_modelo` ASC) VISIBLE,
  INDEX `fk_COCHE_PROVEEDOR1_idx` (`FK_id_proveedor` ASC) VISIBLE,
  INDEX `fk_COCHE_SUCURSAL1_idx` (`FK_id_sucursal` ASC) VISIBLE,
  INDEX `fk_COCHE_CLIENTE1_idx` (`FK_CLIENTE_DNI` ASC) VISIBLE,
  CONSTRAINT `fk_COCHE_CLIENTE1`
    FOREIGN KEY (`FK_CLIENTE_DNI`)
    REFERENCES `tiendacoches`.`cliente` (`DNI`),
  CONSTRAINT `fk_COCHE_MODELO1`
    FOREIGN KEY (`FK_id_modelo`)
    REFERENCES `tiendacoches`.`modelo` (`id_modelo`),
  CONSTRAINT `fk_COCHE_PROVEEDOR1`
    FOREIGN KEY (`FK_id_proveedor`)
    REFERENCES `tiendacoches`.`proveedor` (`id_proveedor`),
  CONSTRAINT `fk_COCHE_SUCURSAL1`
    FOREIGN KEY (`FK_id_sucursal`)
    REFERENCES `tiendacoches`.`sucursal` (`id_sucursal`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendacoches`.`sucursal_cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendacoches`.`sucursal_cliente` (
  `FK_id_sucursal` INT(11) NOT NULL,
  `FK_CLIENTE_DNI` VARCHAR(9) NOT NULL,
  `fecha` DATE NOT NULL,
  PRIMARY KEY (`FK_id_sucursal`, `FK_CLIENTE_DNI`),
  INDEX `fk_SUCURSAL_has_CLIENTE_CLIENTE1_idx` (`FK_CLIENTE_DNI` ASC) VISIBLE,
  INDEX `fk_SUCURSAL_has_CLIENTE_SUCURSAL1_idx` (`FK_id_sucursal` ASC) VISIBLE,
  CONSTRAINT `fk_SUCURSAL_has_CLIENTE_CLIENTE1`
    FOREIGN KEY (`FK_CLIENTE_DNI`)
    REFERENCES `tiendacoches`.`cliente` (`DNI`),
  CONSTRAINT `fk_SUCURSAL_has_CLIENTE_SUCURSAL1`
    FOREIGN KEY (`FK_id_sucursal`)
    REFERENCES `tiendacoches`.`sucursal` (`id_sucursal`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tiendacoches`.`trabajadores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendacoches`.`trabajadores` (
  `DNI` VARCHAR(9) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `años_antiguedad` INT(11) NOT NULL,
  `salario` FLOAT NOT NULL,
  `jornada` VARCHAR(45) NOT NULL,
  `telefono` INT NOT NULL,
  `fecha_nacimiento` DATE NOT NULL,
  `pais_nacimiento` VARCHAR(45) NOT NULL,
  `FK_id_sucursal` INT(11) NOT NULL,
  PRIMARY KEY (`DNI`),
  UNIQUE INDEX `DNI_UNIQUE` (`DNI` ASC) VISIBLE,
  INDEX `fk_TRABAJADORES_SUCURSAL1_idx` (`FK_id_sucursal` ASC) VISIBLE,
  CONSTRAINT `fk_TRABAJADORES_SUCURSAL1`
    FOREIGN KEY (`FK_id_sucursal`)
    REFERENCES `tiendacoches`.`sucursal` (`id_sucursal`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
